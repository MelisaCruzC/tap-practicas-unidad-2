package mx.edu.itver.topicos;

import javax.swing.*;
import java.awt.*;

public class MarcoConImagen extends JFrame{
    public MarcoConImagen(){
        super ("Colores, Fuentes e Imagenes");
        setSize(400,400);
        setLocation(100,100);
        getContentPane().setBackground(Color.cyan);
        
        MiPanel jspanel = new MiPanel();
        jspanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        jspanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        add (jspanel);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
    }
    class MiPanel extends JScrollPane{
        public void paint(Graphics g){
            Font f1 = new Font("Arial",Font.BOLD,24);
            Font f2 = new Font("Courier",Font.BOLD | Font.ITALIC, 18);
            
            String archivo = "colash.jpg";
            Image image = getToolkit().getImage(archivo);
            g.setColor(Color.blue);
            g.drawString("Mensaje en azul", 100, 10);
            g.setColor(new Color(141,17,20));
            g.drawString("Mensaje en un nuevo color", 100, 40);
            g.setFont(f1);
            g.setColor(new Color(24,102,33));
            g.drawString("Mensaje en Arial", 100, 80);
            g.setFont(f2);
            g.drawString("Mensaje en Courier", 100, 120);
            g.drawImage(image, 100, 160, 400, 400, this);            
        }
    }
    public static void main(String [] args){
        MarcoConImagen inicia = new MarcoConImagen();
    }        
}
