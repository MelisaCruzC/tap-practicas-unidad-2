/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.itver.topicos;

/**
 *
 * @author Laboratorio
 */
import java.awt.*;
import javax.swing.*;

public class ImageComponent extends JComponent {
  Image image;
  Dimension size;
  
  public ImageComponent(Image image) {
    this.image = image;
    MediaTracker mt = new MediaTracker(this);
    mt.addImage(image, 0);
    try {
      mt.waitForAll( );
    }
    catch (InterruptedException e) {
      // error ...
    };

    size = new Dimension (image.getWidth(null),
                          image.getHeight(null));
    setSize(size);
  }
  
  public void paint(Graphics g) {
    g.drawImage(image, 0, 0, this);
  }
  
  public Dimension getPreferredSize( ) {
    return size;
  }
}
