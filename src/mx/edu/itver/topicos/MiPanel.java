/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.itver.topicos;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;

/**
 *
 * @author Laboratorio
 */
public class MiPanel extends JPanel{

        private Image image;
        private String archivo;
        
        public MiPanel(){
           this.archivo = null;
        }
        
        public MiPanel(String _archivo){
            this.archivo = _archivo;
        }
        
        public void setArchivo(String _archivo){
            this.archivo = _archivo;
        }
        
        @Override
        public void paint(Graphics g){
            if (archivo!=null){
               image = getToolkit().getImage(archivo);
                g.drawImage(image, 0, 0,this.getWidth(),this.getHeight(), this);            
            }
        }
    }
